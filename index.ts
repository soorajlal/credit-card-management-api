'use strict'

import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import cors from 'cors';
import indexRouter from './app/routers/indexRouter';
import usersRouter from './app/routers/usersRouter';
import cardsRouter from './app/routers/cardsRouter';

import sequelize from './app/config/database';

dotenv.config();

const app = express();
const port = process.env.PORT || 8080;

sequelize.sync({force: false});

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/cards', cardsRouter);

//handle 404
app.use((req, res)=>{
    res.status(404).json({
        error: true,
        message: 'Not found!!'
    })
});

app.listen(port, ()=>{
    console.log(app.settings.env);
    console.log(`listening to port ${port}`);
})

export default app;
