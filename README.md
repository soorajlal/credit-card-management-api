# Credit Card Management API
An application that with
  - signup API
  - login API
  - credit card create API
  - credit card list API

### Requirements
  - Node.js v13

### Installation
Install the dependencies and devDependencies and start the server.

```sh
$ cd credit-card-management-api
$ npm install
$ npm start
```

To run lint,
```sh
$ npm run lint
```

To run test cases,
```sh
$ npm run test
```

### Configuration
  - Copy .env.example to .env.
  - Create an empty database in PostgreSQL.
  - Open .env file in any editor and set your database details.