'use strict'

import client from './dbconfig';

client.connect().then(() => {
    client.query('CREATE DATABASE '+process.env.DB_TEST_DATABASE).then(() => {
        
    }).catch(err => {
        console.log(err);
    }).finally(() => {
        client.end();
    });
}).catch(err => {
    console.log(err);
});