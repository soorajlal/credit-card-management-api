'use strict'

import {Client} from 'pg';
import dotenv from 'dotenv';
dotenv.config();

const connectionString = 'postgres://'+process.env.DB_USER+':'+process.env.DB_PASSWORD+'@'+process.env.DB_HOST;
const client = new Client({
    connectionString: connectionString
});

export default client;