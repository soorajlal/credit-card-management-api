'use strict'

import 'mocha';
import chai from 'chai';
import chaiHttp from 'chai-http';
import {User} from '../app/models/user';
import {Card} from '../app/models/card';
import app from '../index';
import {Sequelize} from 'sequelize-typescript';

chai.use(chaiHttp);

const expect = chai.expect;

describe('Test cases:', ()=> {

    before((done)=>{      

        //connect test db
        const sequelize =  new Sequelize({
            database: process.env.DB_TEST_DATABASE ,
            dialect: 'postgres',
            username: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            host: process.env.DB_HOST,
            storage: ':memory:',
            models: [User, Card],
            logging:false
        });

        sequelize.sync({force: true})
        .then(()=> {
            done();            
        })
        .catch((err)=> {
            console.log(err);
            done(err)
        });
     
    });
    describe('users:', ()=> {
        describe('POST /users/signup:', ()=> {

            it('Should return 400 and error message if email address is empty on /users/signup POST', (done)=> {
                const payload = {
                    'email': '', 
                    'password': '123456', 
                    'phoneNumber':'9999999999'
                };

                chai.request(app)
                    .post('/users/signup')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Email is required');
                        done();
                });
            });

            it('Should return 400 and error message if email address is invalid on /users/signup POST', (done)=> {
                const payload = {
                    'email': 'test', 
                    'password': '123456', 
                    'phoneNumber':'9999999999'
                };

                chai.request(app)
                    .post('/users/signup')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Email is invalid');
                        done();
                });
            });

            it('Should return 400 and error message if password is empty on /users/signup POST', (done)=> {
                const payload = {
                    'email': 'unittest@test.com', 
                    'password': '', 
                    'phoneNumber':'9999999999'
                };

                chai.request(app)
                    .post('/users/signup')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Password is required');
                        done();
                });
            });

            it('Should return 400 and error message if password contains space on /users/signup POST', (done)=> {
                const payload = {
                    'email': 'unittest@test.com', 
                    'password': '12 34', 
                    'phoneNumber':'9999999999'
                };

                chai.request(app)
                    .post('/users/signup')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('No spaces are allowed in the password');
                        done();
                });
            });

            it('Should return 400 and error message if password length is less than six characters on /users/signup POST', (done)=> {
                const payload = {
                    'email': 'unittest@test.com', 
                    'password': '1234', 
                    'phoneNumber':'9999999999'
                };

                chai.request(app)
                    .post('/users/signup')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Password should be between 6-50 characters');
                        done();
                });
            });

            it('Should return 400 and error message if phoneNumber is empty on /users/signup POST', (done)=> {
                const payload = {
                    'email': 'unittest@test.com', 
                    'password': '123456', 
                    'phoneNumber':''
                };

                chai.request(app)
                    .post('/users/signup')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Phone number is required');
                        done();
                });
            });

            it('Should return 400 and error message if phoneNumber length is less than 10 characters on /users/signup POST', (done)=> {
                const payload = {
                    'email': 'unittest@test.com', 
                    'password': '123456', 
                    'phoneNumber':'999999999'
                };

                chai.request(app)
                    .post('/users/signup')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        //console.log(JSON.stringify(res));
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Phone number should be 10 characters long');
                        done();
                });
            });

            it('Should return 400 and error message if phoneNumber contains non numeric characters on /users/signup POST', (done)=> {
                const payload = {
                    'email': 'unittest@test.com', 
                    'password': '123456', 
                    'phoneNumber':'999-999-9999'
                };

                chai.request(app)
                    .post('/users/signup')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        //console.log(JSON.stringify(res));
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Invalid phone number');
                        done();
                });
            });

            it('Should return 201 and access token if details are valid on /users/signup POST', (done)=> {
                const payload = {
                    'email': 'unittest@test.com', 
                    'password': '123456', 
                    'phoneNumber':'9999999999'
                };

                chai.request(app)
                    .post('/users/signup')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        //console.log(JSON.stringify(res));
                        expect(res).to.have.status(201);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.false;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('User account created successfully');
                        expect(res.body).to.have.property('data');
                        expect(res.body.data).to.be.a('object');
                        expect(res.body.data).to.have.property('access_token');
                        expect(res.body.data.access_token).to.have.lengthOf.at.least(1);
                        done();
                });
                    
            });

            it('Should return 422 and error message if email address is already exist on /users/signup POST', (done)=> {
                const payload = {
                    'email': 'unittest@test.com', 
                    'password': '123456', 
                    'phoneNumber':'9999999999'
                };

                chai.request(app)
                    .post('/users/signup')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        //console.log(JSON.stringify(res));
                        expect(res).to.have.status(422);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Email already exists');
                        done();
                });
            });

        });

        describe('POST /users/login:', ()=> {

            it('Should return 400 and error message if email address is empty on /users/login POST', (done)=> {
                const payload = {
                    'email': '', 
                    'password': '123456'
                };

                chai.request(app)
                    .post('/users/login')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Email is required');
                        done();
                });
            });

            it('Should return 400 and error message if password is empty on /users/login POST', (done)=> {
                const payload = {
                    'email': 'unittest@test.com', 
                    'password': ''
                };

                chai.request(app)
                    .post('/users/login')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Password is required');
                        done();
                });
            });

            it('Should return 200 and access token if email & password are valid on /users/login POST', (done)=> {
                const payload = {
                    'email': 'unittest@test.com', 
                    'password': '123456'
                };

                chai.request(app)
                    .post('/users/login')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(200);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.false;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('User logged in successfully');
                        expect(res.body).to.have.property('data');
                        expect(res.body.data).to.be.a('object');
                        expect(res.body.data).to.have.property('access_token');
                        expect(res.body.data.access_token).to.have.lengthOf.at.least(1);
                        done();
                });
                    
            });

            it('Should return 401 and error message if email address does not exist on /users/login POST', (done)=> {
                const payload = {
                    'email': 'unittest123@test.com', 
                    'password': '123456'
                };

                chai.request(app)
                    .post('/users/login')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(401);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Incorrect email address');
                        done();
                });
            });

            it('Should return 401 and error message if password is incorrect on /users/login POST', (done)=> {
                const payload = {
                    'email': 'unittest@test.com', 
                    'password': '123456123'
                };

                chai.request(app)
                    .post('/users/login')
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(401);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Incorrect password');
                        done();
                });
            });
        });
    });

    describe('cards:', ()=> {
        const payload = {
            'email': 'unittest@test.com', 
            'password': '123456'
        };
    
        let jwtToken = '';
        before((done)=>{
            chai.request(app)
            .post('/users/login')
            .send(payload)
            .then((res)=>{
                
                expect(res).to.have.status(200);
                expect(res).to.be.a('object');
                expect(res.body).to.have.property('error');
                expect(res.body.error).to.be.false;
                expect(res.body).to.have.property('message');
                expect(res.body.message).to.have.lengthOf.at.least(1);
                expect(res.body.message).to.equal('User logged in successfully');
                expect(res.body).to.have.property('data');
                expect(res.body.data).to.be.a('object');
                expect(res.body.data).to.have.property('access_token');
                expect(res.body.data.access_token).to.have.lengthOf.at.least(1);
                jwtToken = res.body.data.access_token;
                done();
            });
        });

        describe('POST /cards:', ()=> {
            it('Should return 400 and error message if card owner name is empty on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: '', 
                    cardNumber: '411111111111', 
                    expiryMonth: '12',
                    expiryYear: '2021',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Card owner name is required');
                        done();
                });
            });

            it('Should return 400 and error message if card number is empty on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '', 
                    expiryMonth: '12',
                    expiryYear: '2021',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Card number is required');
                        done();
                });
            });

            it('Should return 400 and error message if card number is less than 13 characters on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '411111111111', 
                    expiryMonth: '12',
                    expiryYear: '2021',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Card number should be between 13-19 characters');
                        done();
                });
            });

            it('Should return 400 and error message if card number is invalid on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '5111111111111111', 
                    expiryMonth: '12',
                    expiryYear: '2021',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Invalid card number');
                        done();
                });
            });

            it('Should return 400 and error message if expiry month is empty on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '4111111111111111', 
                    expiryMonth: '',
                    expiryYear: '2021',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Expiry month is required');
                        done();
                });
            });

            it('Should return 400 and error message if expiry month is non numeric on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '4111111111111111', 
                    expiryMonth: 'ab',
                    expiryYear: '2021',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Invalid expiry month');
                        done();
                });
            });

            it('Should return 400 and error message if expiry month is past month on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '4111111111111111', 
                    expiryMonth: '03',
                    expiryYear: '2020',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Invalid expiry month');
                        done();
                });
            });

            it('Should return 400 and error message if expiry month format is invalid on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '4111111111111111', 
                    expiryMonth: '6',
                    expiryYear: '2020',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Expiry month should be MM format');
                        done();
                });
            });

            it('Should return 400 and error message if expiry year is empty on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '4111111111111111', 
                    expiryMonth: '12',
                    expiryYear: '',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Expiry year is required');
                        done();
                });
            });

            it('Should return 400 and error message if expiry year is non numeric on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '4111111111111111', 
                    expiryMonth: '12',
                    expiryYear: 'abcd',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Invalid expiry year');
                        done();
                });
            });

            it('Should return 400 and error message if expiry year is past year on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '4111111111111111', 
                    expiryMonth: '12',
                    expiryYear: '2019',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Invalid expiry year');
                        done();
                });
            });

            it('Should return 400 and error message if expiry year format is invalid on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '4111111111111111', 
                    expiryMonth: '12',
                    expiryYear: '20',
                    cardCode: '123'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Expiry year should be YYYY format');
                        done();
                });
            });

            it('Should return 400 and error message if card code is empty on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '4111111111111111', 
                    expiryMonth: '12',
                    expiryYear: '2021',
                    cardCode: ''
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Card code is required');
                        done();
                });
            });

            it('Should return 400 and error message if card code is invalid on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '4111111111111111', 
                    expiryMonth: '12',
                    expiryYear: '2021',
                    cardCode: 'abc'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Invalid card code');
                        done();
                });
            });

            it('Should return 400 and error message if card code length is not 3 on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '4111111111111111', 
                    expiryMonth: '12',
                    expiryYear: '2021',
                    cardCode: '1234'
                };

                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Card code should be 3 characters long');
                        done();
                });
            });

            it('Should return 201 and save card details if provided details are valid on /cards POST', (done)=> {
                const payload = {
                    cardOwnerName: 'Unit Test', 
                    cardNumber: '5500000000000004', 
                    expiryMonth: '12',
                    expiryYear: '2021',
                    cardCode: '123'
                };
                chai.request(app)
                    .post('/cards')
                    .set('Authorization', 'Bearer '+jwtToken)
                    .send(payload)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        
                        expect(res).to.have.status(201);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.false;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Card saved successfully');
                        expect(res.body).to.have.property('data');
                        expect(res.body.data).to.be.a('object');
                        expect(res.body.data).to.have.property('id');
                        expect(res.body.data.id).to.be.at.least(1);
                        expect(res.body.data).to.have.property('cardOwnerName');
                        expect(res.body.data.cardOwnerName).to.have.lengthOf.at.least(3);
                        expect(res.body.data).to.have.property('expiryMonth');
                        expect(res.body.data.expiryMonth).to.have.lengthOf.at.least(2);
                        expect(res.body.data).to.have.property('expiryYear');
                        expect(res.body.data.expiryYear).to.have.lengthOf.at.least(4);
                        expect(res.body.data).to.have.property('lastDigits');
                        expect(res.body.data.lastDigits).to.have.lengthOf.at.least(4);
                        done();
                });
            });

        });

        describe('GET /cards:', ()=> {
            it('Should return 400 and error message if offset is empty on /cards GET', (done)=> {
                const offset = '';
                const limit = 5;
                chai.request(app)
                    .get('/cards?offset='+offset+'&limit='+limit)
                    .set('Authorization', 'Bearer '+jwtToken)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Offset is required');
                        done();
                });
            });

            it('Should return 400 and error message if offset is non numeric on /cards GET', (done)=> {
                const offset = 'a';
                const limit = 5;
                chai.request(app)
                    .get('/cards?offset='+offset+'&limit='+limit)
                    .set('Authorization', 'Bearer '+jwtToken)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Invalid offset');
                        done();
                });
            });

            it('Should return 400 and error message if limit is empty on /cards GET', (done)=> {
                const offset = 0;
                const limit = '';
                chai.request(app)
                    .get('/cards?offset='+offset+'&limit='+limit)
                    .set('Authorization', 'Bearer '+jwtToken)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Limit is required');
                        done();
                });
            });

            it('Should return 400 and error message if limit is non numeric on /cards GET', (done)=> {
                const offset = 0;
                const limit = 'a';
                chai.request(app)
                    .get('/cards?offset='+offset+'&limit='+limit)
                    .set('Authorization', 'Bearer '+jwtToken)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(400);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.true;
                        expect(res.body).to.have.property('message');
                        expect(res.body.message).to.have.lengthOf.at.least(1);
                        expect(res.body.message).to.equal('Invalid limit');
                        done();
                });
            });

            it('Should return 200 and cards if valid offset and limit are provided on /cards GET', (done)=> {
                const offset = 0;
                const limit = 5;
                chai.request(app)
                    .get('/cards?offset='+offset+'&limit='+limit)
                    .set('Authorization', 'Bearer '+jwtToken)
                    .end((err, res) => {
                        if (err) {
                            throw err;
                        }
                        expect(res).to.have.status(200);
                        expect(res).to.be.a('object');
                        expect(res.body).to.have.property('error');
                        expect(res.body.error).to.be.false;
                        expect(res.body).to.have.property('data');
                        expect(res.body.data).to.be.a('object');
                        expect(res.body.data).to.have.property('current');
                        expect(res.body.data.current).to.be.at.least(0);
                        expect(res.body.data).to.have.property('pages');
                        expect(res.body.data.pages).to.be.at.least(1);
                        expect(res.body.data).to.have.property('total_items');
                        expect(res.body.data.total_items).to.be.at.least(1);
                        expect(res.body.data).to.have.property('cards');
                        expect(res.body.data.cards).to.be.an('array');
                        expect(res.body.data.cards[0].id).to.exist;
                        expect(res.body.data.cards[0].id).to.be.at.least(1);
                        expect(res.body.data.cards[0].cardOwnerName).to.exist;
                        expect(res.body.data.cards[0].cardOwnerName).to.have.lengthOf.at.least(3);
                        expect(res.body.data.cards[0].lastDigits).to.exist;
                        expect(res.body.data.cards[0].lastDigits).to.have.lengthOf.at.least(4);
                        done();
                });
            });
        });
    });

    after((done) => {
        console.log('Done!!');
        done();
        process.exit(); 
    });
});
