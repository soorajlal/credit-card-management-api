'use strict'

import {Table, Column, Model, Unique} from 'sequelize-typescript';

@Table
export class User extends Model<User> {
 
  @Unique
  @Column
  email!: string;

  @Column
  password!: string;
 
  @Column
  phoneNumber!: string;

}
