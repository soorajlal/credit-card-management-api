import {Table, Column, Model} from 'sequelize-typescript';

@Table
export class Card extends Model<Card> {
 
  @Column
  cardOwnerName!: string;

  @Column
  cardNumber!: string;

  @Column
  expiryMonth!: string;

  @Column
  expiryYear!: string;

  @Column
  cardCode!: string;
   
  @Column
  lastDigits!: string;

  @Column
  userId: number;
}
