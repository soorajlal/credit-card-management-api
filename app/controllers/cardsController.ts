'use strict'

import {Card} from '../models/card';
import * as crypt from '../helpers/crypt';

export const create = (req, res)=>{
    const {cardOwnerName, cardNumber, expiryMonth, expiryYear, cardCode} = req.body;
    const userId = req.id;
    const encryptCardNumber = crypt.encrypt(cardNumber);
    const lastDigits = cardNumber.slice(-4);

    Card.create({
        cardOwnerName: cardOwnerName, 
        cardNumber: encryptCardNumber, 
        expiryMonth: expiryMonth,
        expiryYear: expiryYear,
        cardCode: cardCode,
        lastDigits: lastDigits,
        userId: userId
    })
    .then((card) => {

        const data = {
                        id: card.id, 
                        cardOwnerName: card.cardOwnerName,
                        expiryMonth: card.expiryMonth,
                        expiryYear: card.expiryYear,
                        lastDigits: card.lastDigits
                    };

        return res.status(201).json({
            error: false,
            message: 'Card saved successfully',
            data: data
        });
    })
    .catch((err)=>{
        return res.status(422).json({
            error: true,
            message: 'Unable to process this request',
            err:err
        });
    });

};

export const getAll = (req, res)=>{
    const {offset, limit} = req.query;
    const page = offset * limit;
    const userId = req.id;

    Card.findAndCountAll({
        attributes: ['id', 'cardOwnerName', 'lastDigits'],
        where: {
            userId: userId
        },
        order: [
            ['id', 'ASC']
        ],
        limit: limit,
        offset: page
    })
    .then((result) => {
        
        const totalPages = Math.ceil(result.count/limit);
        const currentPage = parseInt(offset);

        const data = {
                        current: currentPage,
                        pages: totalPages,
                        total_items: result.count,
                        cards: result.rows 
                    };

        return res.status(200).json({
            error: false,
            data: data
        });
    })
    .catch(()=>{
        return res.status(422).json({
            error: true,
            message: 'Unable to process this request'
        });
    });

};
