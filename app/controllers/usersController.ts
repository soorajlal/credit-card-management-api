'use strict'

import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import {User} from '../models/user';
import dotenv from 'dotenv';
dotenv.config();

export const signup = (req, res)=>{
    const {email, password, phoneNumber} = req.body;

    User
    .findOne({
        where: 
        {
            email: email
        }
    })
    .then(result => {

        if (result) {
            return res.status(422).json({
                error: true,
                message: 'Email already exists'
            });
        }

        //hash password
        bcrypt.hash(password, 10, (err, hashedPassword)=> {
            if (err) {
                return res.status(422).json({
                    error: true,
                    message: 'Unable to process this request'
                });
            }

            User.create({
                email: email, 
                password: hashedPassword, 
                phoneNumber: phoneNumber
            })
            .then((user) => {

                //generate token
                const token = jwt.sign(
                    {
                        id: user.id
                    },
                    process.env.JWT_KEY,
                    { 
                        expiresIn: process.env.JWT_TOKEN_EXPIRY //in hours
                    }
                );
            
                const data = {
                    access_token: token
                };
            
                return res.status(201).json({
                    error: false,
                    message: 'User account created successfully',
                    data: data
                });
            })
            .catch(()=>{
                return res.status(422).json({
                    error: true,
                    message: 'Unable to process this request'
                });
            });
        });

    })
    .catch(()=>{
        return res.status(422).json({
            error: true,
            message: 'Unable to process this request'
        });
    }); 

};

export const login = (req, res)=>{
    const {email, password} = req.body;

    //check email exists or not
    User
    .findOne({
        where: 
        {
            email: email
        }
    })
    .then(row => {
        
        if (!row) {
            return res.status(401).json({
                error: true,
                message: 'Incorrect email address'
            });
        }

        //hash password
        bcrypt.compare(password, row.password, (err, result)=> {
            if (err) {
                return res.status(422).json({
                    error: true,
                    message: 'Unable to process this request'
                });
            }

            if (!result) {
                return res.status(401).json({
                    error: true,
                    message: 'Incorrect password'
                });
            }
            

            //generate token
            const token = jwt.sign(
                {
                    id: row.id
                },
                process.env.JWT_KEY,
                { 
                    expiresIn: process.env.JWT_TOKEN_EXPIRY //in hours
                }
            );
        
            const data = {
                access_token: token
            };
        
            return res.status(200).json({
                error: false,
                message: 'User logged in successfully',
                data: data
            });
        });
    })
    .catch(()=>{
        return res.status(422).json({
            error: true,
            message: 'Unable to process this request'
        });
    });    
};
