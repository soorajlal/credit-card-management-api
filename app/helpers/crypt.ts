'use strict'

import * as crypto from 'crypto';

const key = process.env.ENCRYPTION_KEY;

export const encrypt = (text: string) => {
	const iv = crypto.randomBytes(16);
	const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
	const encrypted = cipher.update(text);
	const encryptedText = Buffer.concat([encrypted, cipher.final()]);
	const ivHex = iv.toString('hex');
	const encryptedHex = encryptedText.toString('hex');
	return ivHex+':'+encryptedHex;
};

export const decrypt = (text: string)=> {
	let textParts = text.split(':');
	const iv = Buffer.from(textParts.shift(), 'hex');
	const encryptedText = Buffer.from(textParts.join(':'), 'hex');
	const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
	const decrypted = decipher.update(encryptedText);
	const decryptedText = Buffer.concat([decrypted, decipher.final()]);
	return decryptedText.toString();
};

