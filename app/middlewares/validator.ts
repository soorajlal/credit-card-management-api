'use strict'

import { check, validationResult } from 'express-validator';
import * as cardValidator from 'card-validator';

export const usersSignupValidationRules = () => {
    return [ 
        check('email').trim().notEmpty().withMessage('Email is required').isEmail().withMessage('Email is invalid').isLength({ min: 3, max:100 }).withMessage('Email should be between 3-100 characters'),
        check('password').notEmpty().withMessage('Password is required').custom(value => !/\s/.test(value)).withMessage('No spaces are allowed in the password').isLength({ min: 6, max:50 }).withMessage('Password should be between 6-50 characters'),
        check('phoneNumber').trim().notEmpty().withMessage('Phone number is required').matches(/^[0-9]*$/, 'i').withMessage('Invalid phone number').isLength({ min: 10, max:10 }).withMessage('Phone number should be 10 characters long')
    ]
}

export const usersLoginValidationRules = () => {
    return [ 
        check('email').trim().notEmpty().withMessage('Email is required').isEmail().withMessage('Email is invalid'),
        check('password').notEmpty().withMessage('Password is required')
    ]
}

export const cardsCreateValidationRules = () => {
    const d = new Date();
    const year = d.getFullYear();
    const month = ('0' + (d.getMonth() + 1)).slice(-2);
    return [ 
        check('cardOwnerName').trim().notEmpty().withMessage('Card owner name is required').isLength({ min: 3, max:100 }).withMessage('Card owner name should be between 3-100 characters'),
        check('cardNumber').trim().notEmpty().withMessage('Card number is required').isLength({ min: 13, max:19 }).withMessage('Card number should be between 13-19 characters').custom(value => { const numberValidation = cardValidator.number(value); return numberValidation.isValid; }).withMessage('Invalid card number'),
        check('expiryMonth').trim().notEmpty().withMessage('Expiry month is required').isNumeric().withMessage('Invalid expiry month').isLength({ min: 2, max:2 }).withMessage('Expiry month should be MM format').custom((value, { req }) => !(req.body.expiryYear == year && value < month)).withMessage('Invalid expiry month'),
        check('expiryYear').trim().notEmpty().withMessage('Expiry year is required').isNumeric().withMessage('Invalid expiry year').isLength({ min: 4, max:4 }).withMessage('Expiry year should be YYYY format').custom(value => !(value < year)).withMessage('Invalid expiry year'),
        check('cardCode').trim().notEmpty().withMessage('Card code is required').isNumeric().withMessage('Invalid card code').isLength({ min: 3, max:3 }).withMessage('Card code should be 3 characters long')
    ]
}

export const cardsGetAllValidationRules = () => {
    return [ 
        check('offset').trim().notEmpty().withMessage('Offset is required').isNumeric().withMessage('Invalid offset'),
        check('limit').trim().notEmpty().withMessage('Limit is required').isNumeric().withMessage('Invalid limit')
    ]
}

export const validate = (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        //get first error
        const message = errors.array()[0]['msg'];
    
        return res.status(400).json({
            error: true,
            message: message
        });
    }
    next();
}
