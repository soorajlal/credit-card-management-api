'use strict'

import jwt from 'jsonwebtoken';

const authToken = (req, res, next) => {
    const token = req.headers['authorization'];
    
    if (!token) {
        return res.status(401).json({
            error: true,
            message: 'Access token is missing or invalid'
        })
    }

    const jwtToken = token.replace('Bearer ', '');

    jwt.verify(jwtToken, process.env.JWT_KEY, (err, decoded)=>{
        if (err) 
            return res.status(401).json({
                error: true,
                message: 'Access token is missing or invalid'
            })

        req.id = decoded.id;
        
        next();
    });
}

export default authToken;
