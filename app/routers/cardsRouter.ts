'use strict'

import {Router} from 'express';
import * as cardsController from '../controllers/cardsController'
import authToken from '../middlewares/auth';
import * as validator from '../middlewares/validator';
const router = Router();

router.post('/', authToken, validator.cardsCreateValidationRules(), validator.validate, cardsController.create);
router.get('/', authToken, validator.cardsGetAllValidationRules(), validator.validate, cardsController.getAll);

export default router;
