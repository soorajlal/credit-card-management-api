'use strict'

import {Router} from 'express';
const router = Router();

router.get('/', (req, res)=>{
	res.send('Welcome to Credit Card API');
});

export default router;
