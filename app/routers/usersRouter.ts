'use strict'

import {Router} from 'express';
import * as usersController from '../controllers/usersController'
import * as validator from '../middlewares/validator';
const router = Router();

router.post('/signup', validator.usersSignupValidationRules(), validator.validate, usersController.signup);
router.post('/login', validator.usersLoginValidationRules(), validator.validate, usersController.login);

export default router;
