'use strict'

import {Sequelize} from 'sequelize-typescript';
import {User} from '../models/user';
import {Card} from '../models/card';

const sequelize =  new Sequelize({
        database: process.env.DB_DATABASE,
        dialect: 'postgres',
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        host: process.env.DB_HOST,
        storage: ':memory:',
        models: [User, Card],
        logging:false
});

export default sequelize;
