swagger: "2.0"
info:
  description: "This is a documentation for credit card management project"
  version: "v1"
  title: "Credit Card Management API"
host: "localhost:8080"
tags:
- name: "user"
  description: "Operations about user"
- name: "card"
  description: "Operations about card"
schemes:
- "http"

paths:
  /users/signup:
    post:
      tags:
      - "user"
      summary: "Create user"
      description: "This can only be done to regsiter a user."
      operationId: "createUser"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        schema:
            $ref: '#/definitions/UserSignup'
      responses:
        "201":
          description: "User account created"
          schema:
            $ref: '#/definitions/SignupCreatedResponse'
        "400":
          description: "Bad request"
          schema:
            $ref: '#/definitions/SignupBadRequestResponse'
        "422":
          description: "Email already exists"
          schema:
            $ref: '#/definitions/SignupUnprocessableEntityResponse'
  
  /users/login:
    post:
      tags:
      - "user"
      summary: "Login user"
      description: "This can only be done to login a user."
      operationId: "loginUser"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        schema:
            $ref: '#/definitions/UserLogin'
      responses:
        "200":
          description: "User logged in"
          schema:
            $ref: '#/definitions/LoginSuccessResponse'
        "400":
          description: "Bad request"
          schema:
            $ref: '#/definitions/LoginBadRequestResponse'
        "401":
          description: "Invalid credentials"
          schema:
            $ref: '#/definitions/LoginUnauthorizedResponse'
            
  /cards:
    post:
      tags:
      - "card"
      summary: "Create credit card"
      description: "This can only be done to create credit card."
      operationId: "createCard"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "Authorization"
        in: "header"
        required: true
        type: "string"
      - in: "body"
        name: "body"
        schema:
            $ref: '#/definitions/Card'
      responses:
        "201":
          description: "Card created"
          schema:
            $ref: '#/definitions/CardCreatedResponse'
        "400":
          description: "Bad request"
          schema:
            $ref: '#/definitions/CardBadRequestResponse'
        "401":
          description: "Invalid token"
          schema:
            $ref: '#/definitions/UnauthorizedResponse'
        "422":
          description: "Duplicate Card"
          schema:
            $ref: '#/definitions/CardUnprocessableEntityResponse'
  
    get:
      tags:
      - "card"
      summary: "Get all credit cards"
      description: "This can only be done to get all credit cards."
      operationId: "getCard"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "Authorization"
        in: "header"
        required: true
        type: "string"
      - name: "offset"
        in: "query"
        required: true
        type: "integer"
        minimum: 0
        description: "The number of items to skip before starting to collect the result set"
      - name: "limit"
        in: "query"
        required: true
        type: "integer"
        minimum: 1
        description: "The numbers of items to return"

      responses:
        "200":
          description: "Card success response"
          schema:
            $ref: '#/definitions/CardSuccessResponse'
        "401":
          description: "Invalid token"
          schema:
            $ref: '#/definitions/UnauthorizedResponse'


      
definitions:
  
  UserSignup:
    type: "object"
    properties:
      email:
        type: "string"
        minLength: 2
        example: "test@test.com"
      password:
        type: "string"
        minLength: 4
        example: "123456"
      phoneNumber:
        type: "string"
        minLength: 7
        maxLength: 15
        example: "9874563210"
    xml:
      name: "User"
  
  UserLogin:
    type: "object"
    properties:
      email:
        type: "string"
        minLength: 2
        example: "test@test.com"
      password:
        type: "string"
        minLength: 4
        example: "123456"
        
  Card:
    type: "object"
    properties:
      cardOwnerName:
        type: "string"
        minLength: 2
        example: "Test User"
      cardNumber:
        type: "string"
        minLength: 14
        maxLength: 17
        example: "5424000000000015"
      expiryMonth:
        type: "integer"
        minLength: 2
        example: 12
      expiryYear:
        type: "integer"
        minLength: 4
        example: 2021
      cardCode:
        type: "string"
        minLength: 3
        maxLength: 4
        example: "999"
  
  SignupCreatedResponse:
    type: "object"
    properties:
      error:
        type: "boolean"
        example: false
      message:
        type: "string"
        example: "User account created successfully"
      data:
       type: "object"
       properties:
        access_token:
          type: "string"
          example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNTg1MDM5MTg3LCJleHAiOjE1ODUxMjU1ODd9.Z71CzssfNM45Bez_GNoX3w-F6rXP1k8llSVV9GKDOjM"
        
  SignupBadRequestResponse:
    type: "object"
    properties:
      error:
        type: "boolean"
      message:
        type: "string"
        example: "Email is required"
        
  SignupUnprocessableEntityResponse:
    type: "object"
    properties:
      error:
        type: "boolean"
      message:
        type: "string"
        example: "Email already exists"
        
  LoginSuccessResponse:
    type: "object"
    properties:
      error:
        type: "boolean"
        example: false
      message:
        type: "string"
        example: "User logged in successfully"
      data:
       type: "object"
       properties:
        access_token: 
          type: "string"
          example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNTg1MDM5MTg3LCJleHAiOjE1ODUxMjU1ODd9.Z71CzssfNM45Bez_GNoX3w-F6rXP1k8llSVV9GKDOjM"
      
  LoginBadRequestResponse:
    type: "object"
    properties:
      error:
        type: "boolean"
      message:
        type: "string"
        example: "Email is required"
        
  LoginUnauthorizedResponse:
    type: "object"
    properties:
      error:
        type: "boolean"
      message:
        type: "string"
        example: "Invalid credentials"
        
  CardCreatedResponse:
    type: "object"
    properties:
      error:
        type: "boolean"
        example: false
      message:
        type: "string"
        example: "Card created successfully"
      data:
       type: "object"
       properties:
        id: 
          type: "integer"
          example: 10
        cardOwnerName:
          type: "string"
          example: "Test 1"
        lastDigits:
          type: "string"
          example: "1234"
              
  CardBadRequestResponse:
    type: "object"
    properties:
      error:
        type: "boolean"
      message:
        type: "string"
        example: "Card number is required"
        
  CardUnprocessableEntityResponse:
    type: "object"
    properties:
      error:
        type: "boolean"
      message:
        type: "string"
        example: "Card already exists"
        
  CardSuccessResponse:
    type: "object"
    properties:
      error:
        type: "boolean"
        example: false
      data:
       type: "object"
       properties:
        current: 
          type: "integer"
          example: 1
        pages:
          type: "integer"
          example: 50
        total_items:
          type: "integer"
          example: 250
        cards:
          type: "array"
          items:
            type: "object"
            properties:
              id: 
                type: "integer"
              cardOwnerName:
                type: "string"
              lastDigits:
                type: "string"
          example: 
          - id: 10
            cardOwnerName: "Test 1"
            lastDigits: "1234"
          - id: 11
            cardOwnerName: "Test 2"
            lastDigits: "7854"
          - id: 12
            cardOwnerName: "Test 3"
            lastDigits: "1248"
          - id: 13
            cardOwnerName: "Test 4"
            lastDigits: "9853"
          - id: 14
            cardOwnerName: "Test 5"
            lastDigits: "4238"  
  
  UnauthorizedResponse:
    type: "object"
    properties:
      error:
        type: "boolean"
      message:
        type: "string"
        example: "Access token is missing or invalid"
        
        
